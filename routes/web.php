<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', 'WelcomeController@welcome')->name('home');
Route::post('save-user', 'SaveUserController@saveUser')->name('save_user');
Route::get('edit-user/{id}', 'SaveUserController@editUser')->name('edit_user');
Route::post('update-user/{id}', 'SaveUserController@updateUser')->name('update_user');
Route::get('delete-user/{id}', 'SaveUserController@deleteUser')->name('delete_user');

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Project</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


    </head>
    <body>
        <section class="container-fluid" style="max-width: 700px; margin: 0 auto; margin-top: 100px;">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                <div class="flex-center position-ref full-height">

                    <div class="content">
                        <h1 class="title m-b-md">
                            Project - Edit User
                        </h1>

                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            {{Session::get('success')}}
                        </div><br>
                        @elseif(Session::has('fail'))
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            {{Session::get('fail')}}
                        </div><br>
                        @endif

                        <form id="project-edit-form" action="{{ route('update_user', ['id' => $user->id]) }}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>
                                    Title
                                </label>
                                <input class="form-control" type="text" name="title" value="{{$user->title}}" />
                            </div>
                        
                            <label>
                                Name
                            </label>
                            <input class="form-control" type="text" name="name" value="{{$user->name}}" /><br><br>
                            <button class="btn btn-primary btn-block" name="submit_btn" >Update</button>
                        </form>
                    </div>
                </div>
                </div>
            </div>
        </section>
        
    </body>
</html>

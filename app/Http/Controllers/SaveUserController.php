<?php

namespace App\Http\Controllers;

use App\UserInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SaveUserController extends Controller
{

    public function saveUser(Request $request)
    {
        $title = $request->title;
        $name = $request->name;

        $user = UserInfo::create([
            'title' => $title,
            'name' => $name
        ]);

        return view('success');
        // return response()->json($user->toArray());
    }

    public function updateUser(Request $request, $id)
    {
        $user = UserInfo::find($id);

        $user->update([
            'title' => $request->title,
            'name' => $request->name
        ]);

        //set flash cookie
        $request->session()->flash('success', 'data updated successfully!');
        return redirect()->back();
        // return response()->json($user->toArray());
    }

    public function editUser(Request $request, $id) {
        $user = UserInfo::find($id);

        return view('edit_user')->with('user', $user);
    }

    public function deleteUser(Request $request, $id) {
        $user = UserInfo::find($id);
        $user->delete();

        $request->session()->flash('success', 'data deleted successfully!');
        return redirect()->back();

    }


}
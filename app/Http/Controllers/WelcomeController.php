<?php

namespace App\Http\Controllers;

use App\UserInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class WelcomeController extends Controller
{
    public function welcome()
    {
        $users_info = UserInfo::all();

        return view('welcome')->with('users_info', $users_info);

    }

}